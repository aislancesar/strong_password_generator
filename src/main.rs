use std::collections::hash_map::DefaultHasher;
use std::env;
use std::hash::{Hash, Hasher};
use rand::prelude::*;
use rand_chacha::ChaCha8Rng;

const LOWER : &str = "abcdefghijklmnopqrstuvwxyz";
const UPPER : &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const NUMS : &str = "0123456789";
const SYMBOLS : &str = "~`!@#$%^&*()_-+={[}]|:;'<,>.?/";

fn main() {
    let args: Vec<String> = env::args().collect();
    let inputed : &str = &args[1];
    let password_size : u64 = args[2].parse::<u64>().unwrap();

    let mut s : DefaultHasher = DefaultHasher::new();
    inputed.hash(&mut s);
    let seed : u64 = s.finish();
    let mut rng = ChaCha8Rng::seed_from_u64(seed);

    let mut password = "".to_owned();
    for n in 1..password_size {
        let set = [NUMS, UPPER, LOWER, SYMBOLS][rng.gen_range(0..4)];
        let chr = set.chars().nth(rng.gen_range(0..set.len())).unwrap();
        password.push(chr);
    }

    println!("{password}")
}
